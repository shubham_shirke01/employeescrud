<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Employee CRUD</title>
      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

   </head>
   <body>
      <div class="container">
         @yield('content')
      </div>

      <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"></script>
      <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
      <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" type="text/js"></script>
   </body>
</html>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<!-- <script src="https://code.jquery.com/jquery-3.5.1.min.js" crossorigin="anonymous"></script>
 --><script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
 <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

<script type="text/javascript">
   $(document).ready(function() {
    $(document).on('click', '#del-employee-ajx', function(e){
         var r=confirm("Are you sure?");
            if (r)
            {
                e.preventDefault();
                var id = $(this).data('id');
                console.log(id);
                     $.ajax({
                       url: '/employee/delete/'+id,
                       type: 'POST',
                       dataType: "json",
                       data: { id:id,
                             _token: "{{csrf_token()}}"
                       },
                       success: function(response){
                        console.log(response);
                         if(response == 1){
                                $("#notice")
                               .show()
                               .html('<div class="alert alert-warning"<strong>Successfully !</strong> record deleted.</div>')
                               .fadeOut(5000);
                               setTimeout(function() 
                                  {
                                    location.reload(); 
                                  }, 2000);
                            }
                       }
                });
 
            }
    });
   });


$(document).ready(function () {

 $("body").on("click","#deleteEmployeee",function(e){

    if(!confirm("Do you really want to do this?")) {
       return false;
     }

    e.preventDefault();
    var id = $(this).data("id");
    // var id = $(this).attr('data-id');
    var token = $("meta[name='csrf-token']").attr("content");
    var url = e.target;

    $.ajax(
        {
          url: "/employee/delete/"+id, //or you can use url: "company/"+id,
          type: 'POST',
          data: {
            _token: token,
                id: id
        },
        success: function (response){

            $("#success").html(response.message)

            Swal.fire(
              'Remind!',
              'Employee deleted successfully!',
              'success'
            )
        }
     });
      return false;
   });
    

});

</script>