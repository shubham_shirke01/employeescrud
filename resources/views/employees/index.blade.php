@extends('employees.layouts')

@section('content')

<style>
  .push-top {
    margin-top: 50px;
  }

 h1 {
    text-align:center;
}

input.right {
        float: right;
      }
</style>

<div class="push-top">
  @if ($errors->any())
        <div class="alert alert-danger" role="alert">
             @foreach ($errors->all() as $error)
                  {{ $error }}
                @endforeach
        </div>
        @elseif(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <br />
    <h1 style="position: center;">Employee List</h1>
  <a class="pull-right" href="{{route('employee.create')}}"><input type="button" name="submit" class="btn btn-success rounded-pill px-4 right" value="Add Employee"></a>
   <a class="pull-right" href="{{route('home')}}"><input type="button" name="submit" class="btn btn-success rounded-pill px-4 right" value="Back Home"></a>
  <div id="notice"></div>
  <meta name="_token" content="{{csrf_token()}}" />
  <table class="table" id="employeeList">
    <thead>
        <tr class="table-warning">
          <td>ID</td>
          <td>Name</td>
          <td>Status</td>
          <td class="text-center">Action</td>
        </tr>
    </thead>
    <tbody>
        @foreach($employees as $employee)
        <tr>
            <td>{{$employee->id}}</td>
            <td>{{$employee->emp_name}}</td>
            <td>{{$employee->emp_status}}</td>
            <td class="text-center">
                <a href="{{ route('employee.edit', $employee->id)}}" class="btn btn-primary btn-sm"">Edit</a>
                <a class="btn btn-danger btn-sm"" id="del-employee-ajx" data-id="{{ $employee->id }}">Delete</a>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
@endsection
