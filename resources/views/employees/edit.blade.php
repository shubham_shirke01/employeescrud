@extends('employees.layouts')

@section('content')

<style>
    .container {
      max-width: 450px;
    }
    .push-top {
      margin-top: 50px;
    }
</style>

<div class="card push-top">
  <div class="card-header">
   Update Employee
  </div>

  <div class="card-body">
    @if ($errors->any())
        <div class="alert alert-danger" role="alert">
             @foreach ($errors->all() as $error)
                  {{ $error }}
                @endforeach
        </div>
        @elseif(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <br />
      <form method="post" action="{{ route('employee.update', $employee->id) }}">
          <div class="form-group">
              @csrf
              @method('PATCH')
              <label for="name">Employee Name</label>
              <input type="text" class="form-control" name="emp_name" value="{{ $employee->emp_name }}"/>
          </div>
          <div class="form-group">
              <label for="email">Status</label>
              <select class="form-control" name="emp_status">
                  <option value="active"{{$employee->emp_status=='active'?'selected':''}}>Active</option>
                  <option value="inactive"{{$employee->emp_status=='inactive'?'selected':''}}>InActive</option>
              </select>
          </div>
           <a href="{{route('employees.index')}}"><input type="button" name="submit" class="btn btn-primary rounded-pill px-4" value="Back"></a>
          <input type="submit" name="submit" class="btn btn-primary rounded-pill px-4" value="Update">
      </form>
  </div>
</div>
@endsection