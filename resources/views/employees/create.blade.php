@extends('employees.layouts')

@section('content')

<style>
    .container {
      max-width: 450px;
    }
    .push-top {
      margin-top: 50px;
    }
</style>

<div class="card push-top">
  <div class="card-header">
    Add Employee
  </div>

  <div class="card-body">
    @if ($errors->any())
        <div class="alert alert-danger" role="alert">
             @foreach ($errors->all() as $error)
                  {{ $error }}
                @endforeach
        </div>
        @elseif(session()->has('success'))
            <div class="alert alert-success">
                {{ session()->get('success') }}
            </div>
        @endif
        <br />
      <form method="post" action="{{ route('employee.store') }}">
          <div class="form-group">
              @csrf
              <label for="name">Employee Name</label>
              <input type="text" class="form-control" name="emp_name"/>
          </div>
        <!--   <button type="submit" class="btn btn-block btn-danger">Create User</button> -->
          <a href="{{route('employees.index')}}"><input type="button" name="submit" class="btn btn-primary rounded-pill px-4" value="Back"></a>
          <input type="submit" name="submit" class="btn btn-primary rounded-pill px-4" value="Submit">
      </form>
  </div>
</div>
@endsection