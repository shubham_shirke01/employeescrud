<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Employee;

class EmployeeController extends Controller
{
     public function index()
   {
        $employees = Employee::paginate(15);
        return view('employees.index',compact('employees'));

    }
    public function create(Request $request)
    {
        return view('employees.create');
    }
    public function store(Request $request)
    {
        try {   
                $request->validate([
                'emp_name' => 'required|unique:employees,emp_name',
                ]);
                 $employee = new Employee;
                 $employee->emp_name = $request->emp_name;
                 $employee->save();

            if ($employee) {
                return redirect()->back()->with('success', 'Employee added successfully');
            }else{
                DB::rollback();
                return redirect()->back()->with('error', 'Can not add employee');
            }
        
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error', $e);
            
        }
        
    }
    public function edit(Request $request,$id)
    {
        try {
            $employee = Employee::findOrFail($id);
            return view('employees.edit',compact('employee'));
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e);
        }
        
    }

    public function update(Request $request,$id)
    {
        try {
            $employee = Employee::findOrFail($id);
            $employee->emp_name = $request->emp_name;
            $employee->emp_status = $request->emp_status;
            $employee->save();
            if ($employee) {
                return redirect()->back()->with('success', 'Employee updated successfully');
            }else{
                return redirect()->back()->with('error', 'Can not update employee');
            }
        } catch (Exception $e) {
            DB::rollback();
            return redirect()->back()->with('error', $e);
        }
        
        
    }
    
    public function delete(Request $request,$id)
    {
        try {
                $employee_god = Employee::find($id)->delete();
                return json_encode($employee_god);
        } catch (Exception $e) {
            return redirect()->back()->with('error', $e);
        }   
    }
}
