<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web 
|-----------------------------Routes---------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/employees', [App\Http\Controllers\EmployeeController::class, 'index'])->name('employees.index')->middleware('auth');
Route::get('/employee/create', [App\Http\Controllers\EmployeeController::class, 'create'])->name('employee.create')->middleware('auth');
Route::post('/employee/store', [App\Http\Controllers\EmployeeController::class, 'store'])->name('employee.store')->middleware('auth');
Route::get('/employee/{id}/edit', [App\Http\Controllers\EmployeeController::class, 'edit'])->name('employee.edit')->middleware('auth');
Route::patch('/employee/{id}/update', [App\Http\Controllers\EmployeeController::class, 'update'])->name('employee.update')->middleware('auth');
Route::delete('/employee/delete/{id}', [App\Http\Controllers\EmployeeController::class, 'delete'])->name('employee.delete')->middleware('auth');